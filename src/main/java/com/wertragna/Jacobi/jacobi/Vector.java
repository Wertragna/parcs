package com.wertragna.Jacobi.jacobi;

import java.io.*;
import java.util.List;

public class Vector implements Serializable {
    private static final long serialVersionUID = 2L;//TODO
    private double array[];

    public Vector(int length) {
        array = new double[length];
    }

    public Vector(double[] arr) {
        array = new double[arr.length];
        for (int i = 0; i < arr.length; ++i) {
            array[i] = arr[i];
        }
    }

    public Vector(List<Double> list) {
        array = new double[list.size()];
        for (int i = 0; i < list.size(); ++i) {
            array[i] = list.get(i);
        }
    }

    public Vector(Vector v) {
        array = new double[v.getLength()];
        for (int i = 0; i < v.getLength(); ++i) {
            array[i] = v.getItem(i);
        }
    }

    public void writeToFile(String fileName) {
        try {
            BufferedWriter bw = new BufferedWriter(new FileWriter(fileName));
            StringBuilder builder = new StringBuilder();
            for (int i = 0; i < array.length; i++) {
                builder.append(array[i] + " ");
            }
            builder.append("\n");
            bw.write(builder.toString());
            bw.close();
        } catch (IOException e) {
            System.out.println("Can not save to file " + fileName + ": " + e);
        }
    }

    public void readFromFile(String fileName) {
        try {
            BufferedReader in = new BufferedReader(new InputStreamReader(
                    new FileInputStream(fileName)));
            String str = in.readLine();
            String[] nums = str.split(" ");
            for (int i = 0; i < nums.length; ++i) {
                array[i] = new Double(nums[i]);
            }
            in.close();
        } catch (Exception e) {
            System.out.println("Can not read from file " + fileName + ": " + e);
        }
    }

    public void setItem(int index, double value) {
        array[index] = value;
    }

    public double getItem(int index) {
        return array[index];
    }

    public int getLength() {
        return array.length;
    }

    public Vector SubVector(int start, int size) {
        double[] res = new double[size];
        for (int i = start, j = 0; j < size; ++i, j++) {
            res[j] = array[i];
        }
        return new Vector(res);
    }

    public void copyPartOfVector(double[] temp, int start, int len) {
        for (int i = start, k = 0; i < start + len; ++i, ++k) {
            array[i] = temp[k];
        }
    }

    public void copyVector(Vector v) {
        if (v.getLength() != array.length) {
            return;
        }
        for (int i = 0; i < array.length; ++i) {
            array[i] = v.getItem(i);
        }
    }
}