package com.wertragna.Jacobi;

import com.wertragna.Jacobi.jacobi.Matrix;
import com.wertragna.Jacobi.jacobi.Vector;
import parcs.AM;
import parcs.AMInfo;

public class JacobiThread implements AM {
    @Override
    public void run(AMInfo info) {
        Matrix a = (Matrix) info.parent.readObject();
        Vector b = (Vector) info.parent.readObject();
        Vector xPrevious = (Vector) info.parent.readObject();
        double[] res = calculate(a, b, xPrevious, info.parent.readInt(), info.parent.readInt());
        info.parent.write(res);
    }

    public double[] calculate(Matrix a, Vector b, Vector xprev, int start, int end) {
        int height = end - start;
        int size = a.getWidth();
        double[] res = new double[height];
        double temp;
        for (int i = start, k = 0; i < end; ++i, k++) {
            temp = b.getItem(i);
            for (int j = 0; j < size; ++j) {
                if (j != i) {
                    temp -= a.getItem(i, j) * xprev.getItem(j);
                }
            }
            temp /= a.getItem(i, i);
            res[k] = temp;
        }
        return res;
    }
}