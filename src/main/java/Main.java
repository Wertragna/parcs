import com.wertragna.Jacobi.MatrixGenerator;
import com.wertragna.Jacobi.jacobi.Matrix;
import com.wertragna.Jacobi.jacobi.Vector;
import parcs.*;

import java.io.IOException;
import java.util.Scanner;


public class Main implements AM {
    private static final int GENERANE_CODE = 1;
    private static int numberOfThreads;
    private static int n;
    private String matrixFileName,
            vectorFileName,
            resultFileName;
    private double EPS = 1e-4;
    private int blocks;
    private Matrix a;
    private Vector b;
    private Vector xPrev;
    private point[] p;
    private channel[] c;
    private Vector xNext;


    public Main() {
        matrixFileName = "matrix" + n + ".txt";
        vectorFileName = "vector" + n + ".txt";
        resultFileName = "result" + n + ".txt";
        blocks = n / numberOfThreads;
        a = new Matrix(n, n);
        b = new Vector(n);
        xPrev = new Vector(n);
        xNext = new Vector(n);
        a.readFromFile(matrixFileName);
        b.readFromFile(vectorFileName);
        p = new point[numberOfThreads];
        p = new point[numberOfThreads];
        c = new channel[numberOfThreads];
    }

    private static void GenerateInputData(int size, int minValue) {
        n = size;
        MatrixGenerator generator = new MatrixGenerator();
        generator.generateInputData(size, size, minValue);
        generator.generateInputData(size, 1, minValue);
    }

    public static void main(String[] args) throws IOException {
        Scanner sc = new Scanner(System.in);
        System.out.println("Choose option 1 - generate, 2 - compute");
        int option = sc.nextInt();
        if (option == GENERANE_CODE) {
            generateData(sc);
        } else
            computeCalculation(sc);
    }

    private static void computeCalculation(Scanner sc) {
        System.out.println("Enter size of the matrix and number of threads");
        n = sc.nextInt();
        numberOfThreads = sc.nextInt();
        if (n % numberOfThreads != 0) {
            System.out.println("Number of rows must be divisible by the number of threads");
        } else
            executeTask();
    }

    private static void executeTask() {
        task currentTask = new task();
        currentTask.addJarFile("jacobi.jar");
        (new Main()).run(new AMInfo(currentTask, (channel) null));
        currentTask.end();
    }

    private static void generateData(Scanner sc) {
        System.out.println("Enter size, minValue");
        int size = sc.nextInt();
        int minValue = sc.nextInt();
        GenerateInputData(size, minValue);
    }

    public double calculateDelta(Vector xPrev, Vector xNext) {
        double sum = 0;
        for (int i = 0; i < xPrev.getLength(); ++i) {
            sum += xNext.getItem(i) - xPrev.getItem(i);
        }
        return Math.abs(sum);
    }

    public void run(AMInfo info) {
        double startTime = System.currentTimeMillis();
        double precision;
        System.out.println("\nComputing...");
        System.out.println("Threads: " + numberOfThreads + ", " + blocks + " rows in each");
        do {
            executeJacopiStepCalculation(info);
            xPrev.copyVector(xNext);
            doChannelCommunication();
            precision = calculateDelta(xPrev, xNext);
        } while (precision > EPS);
        startTime = (System.currentTimeMillis() - startTime) / 1000;
        System.out.println("\nResult found! Saving to file " + resultFileName);
        System.out.println("\nTime: " + startTime);

        xNext.writeToFile(resultFileName);
    }

    private void executeJacopiStepCalculation(AMInfo info) {
        for (int i = 0; i < numberOfThreads; i++) {
            p[i] = info.createPoint();
            c[i] = p[i].createChannel();
            p[i].execute("JacobiThread");
        }
    }

    private void doChannelCommunication() {
        int start = 0, end = 0;
        for (int i = 0; i < numberOfThreads; ++i) {
            end = start + blocks;
            c[i].write(a);
            c[i].write(b);
            c[i].write(xPrev);
            c[i].write(start);
            c[i].write(end);
            double[] temp2 = (double[]) c[i].readObject();
            xNext.copyPartOfVector(temp2, start, blocks);
            start = end - 1;
        }
    }
}